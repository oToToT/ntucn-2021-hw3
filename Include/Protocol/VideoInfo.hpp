#pragma once

#include <cstddef>

struct VideoInfo {
  int Height, Width;
  double Fps;
  size_t FrameCount;
};
