#pragma once

#include <cstddef>
#include <ostream>

#include "picosha2.h"

namespace Packet {

constexpr int MAX_LENGTH = 1000;

struct Header {
  size_t length;
  int seqNumber;
  int ackNumber;
  int fin;
  int syn;
  int ack;
  Header() : length(0), seqNumber(0), ackNumber(0), fin(0), syn(0), ack(0) {}
  friend std::ostream &operator<<(std::ostream &os, const Header &Head) {
    return os << "{length = " << Head.length
              << ", seqNumber = " << Head.seqNumber
              << ", ackNumber = " << Head.ackNumber << ", fin = " << Head.fin
              << ", syn = " << Head.syn << ", ack = " << Head.ack << "}";
  }
};

struct Segment {
  Header header;
  std::byte data[MAX_LENGTH];
  Segment() : header() {}
  friend std::ostream &operator<<(std::ostream &os, const Segment &Seg) {
    return os << "{header = " << Seg.header << ", \ndata = "
              << picosha2::hash256_hex_string(
                     reinterpret_cast<const uint8_t *>(Seg.data),
                     reinterpret_cast<const uint8_t *>(Seg.data +
                                                       Seg.header.length))
              << "}";
  }
};

} // namespace Packet
