#pragma once

#include "Connection/Connection.hpp"
#include "Logger.hpp"

#include <queue>

class Sender : private Connection {
private:
  int SeqNumber;
  std::queue<Packet::Segment> Send;
  std::queue<Packet::Segment> Resend;
  std::queue<Packet::Segment> Unacked;

  size_t Threshold, WindowSize, WindowIncrease;
  void recvAck();
  void writeSegment();

  Logger Log;
  bool Closed;

public:
  Sender(Host RemoteHost, Host BindHost, Logger ActionLogger = {});
  void write(const std::byte *Data, size_t Length);
  bool isClosed() const { return Closed; }
  void close();
  ~Sender() { close(); }
};
