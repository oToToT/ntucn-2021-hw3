#pragma once

#include <arpa/inet.h>

#include <cinttypes>
#include <cstring>
#include <memory>
#include <string>
#include <string_view>

class Host {
  std::string Ip;
  uint16_t Port;
  std::shared_ptr<sockaddr_in> SockAddr;

public:
  Host(std::string HostIp, uint16_t HostPort);
  std::string_view getIp() const { return Ip; }
  uint16_t getPort() const { return Port; }
  std::shared_ptr<sockaddr> getSockAddr() const {
    return std::reinterpret_pointer_cast<sockaddr>(SockAddr);
  }
};
