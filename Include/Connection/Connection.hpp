#pragma once

#include "Connection/Host.hpp"
#include "Protocol/Packet.hpp"

#include <optional>

class Connection {
private:
  Host Remote, Local;
  int SockFd;

protected:
  std::optional<Packet::Segment> recvPacket();
  void sendPacket(const Packet::Segment &Data);

public:
  Connection(Host RemoteHost, Host BindHost);
};
