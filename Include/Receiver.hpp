#pragma once

#include "Connection/Connection.hpp"
#include "Logger.hpp"

#include <optional>
#include <queue>

class Receiver : private Connection {
private:
  int AckNumber;
  std::queue<std::byte> BytesRead;

  std::optional<Packet::Segment> readSegment();

  Logger Log;
  bool Closed;

public:
  Receiver(Host RemoteHost, Host BindHost, Logger ActionLogger = {});
  size_t read(std::byte *Data, size_t Length);
  bool isClosed() const { return Closed; }
};
