#pragma once

#include <ostream>

class Logger {
private:
  std::ostream *Writer;

public:
  Logger(std::ostream *WriterPtr = nullptr) : Writer(WriterPtr) {}

  template <typename T> void log(const T &x) {
    if (Writer != nullptr) {
      (*Writer) << x;
      (*Writer) << std::flush;
    }
  }

  template <typename T, typename... Args>
  void log(const T &x, const Args &...Arguments) {
    if (Writer != nullptr) {
      log(x);
      log(Arguments...);
    }
  }
};
