#include "Connection/Host.hpp"

Host::Host(std::string HostIp, uint16_t HostPort)
    : Ip(HostIp), Port(HostPort), SockAddr(std::make_shared<sockaddr_in>()) {
  std::memset(SockAddr.get(), 0, sizeof(sockaddr_in));
  SockAddr->sin_family = AF_INET;
  SockAddr->sin_addr.s_addr = inet_addr(Ip.c_str());
  SockAddr->sin_port = htons(Port);
}
