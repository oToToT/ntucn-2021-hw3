#include <chrono>
#include <cstring>
#include <iostream>

#include "Connection/Connection.hpp"

Connection::Connection(Host RemoteHost, Host BindHost)
    : Remote(RemoteHost), Local(BindHost),
      SockFd(socket(PF_INET, SOCK_DGRAM, 0)) {

  timeval Timeout{.tv_sec = 0, .tv_usec = 100};
  if (setsockopt(SockFd, SOL_SOCKET, SO_RCVTIMEO, &Timeout, sizeof(timeval)) <
      0) {
    printf("bad setsockopt, reason = %s\n", strerror(errno));
    std::exit(1);
  }

  if (bind(SockFd, Local.getSockAddr().get(), sizeof(sockaddr_in)) < 0) {
    printf("bad bind, reason = %s\n", strerror(errno));
    std::exit(1);
  }
}

std::optional<Packet::Segment> Connection::recvPacket() {
  static socklen_t SockAddrInSize = sizeof(sockaddr_in);
  Packet::Segment Data;
  auto StartTime = std::chrono::high_resolution_clock::now();
  while (recvfrom(SockFd, &Data, sizeof(Packet::Segment), 0,
                  Remote.getSockAddr().get(), &SockAddrInSize) < 0) {
    auto CurrentTime = std::chrono::high_resolution_clock::now();
    if (std::chrono::duration_cast<std::chrono::milliseconds>(CurrentTime -
                                                              StartTime)
            .count() >= 500) {
      return {};
    }
  }
  return Data;
}

void Connection::sendPacket(const Packet::Segment &Data) {
  sendto(SockFd, &Data, sizeof(Packet::Segment), 0, Remote.getSockAddr().get(),
         sizeof(sockaddr_in));
}
