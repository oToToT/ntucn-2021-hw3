.PHONY: all clean Connection

CXX ?= g++
LD = gold
CXXFLAGS = -Ofast -march=native -flto -std=c++17 -fuse-ld=gold -Wall -Wextra -Wshadow -Wconversion -Wwrite-strings
#CXXFLAGS = -g -fsanitize=leak,address,undefined -std=c++17 -fuse-ld=$(LD) -Wall -Wextra -Wshadow -Wconversion -Wwrite-strings
CXXLIBS = -IInclude `pkg-config --cflags --libs opencv`

AR ?= ar
ARFLAGS = rD

all: sender receiver agent

sender: Sender/Main.cpp Sender/Sender.cpp Include/Sender.hpp Include/Logger.hpp Include/Protocol/VideoInfo.hpp connection.a
	$(CXX) $(CXXFLAGS) $(CXXLIBS) Sender/Main.cpp Sender/Sender.cpp connection.a -o sender
receiver: Receiver/Main.cpp Receiver/Receiver.cpp Include/Receiver.hpp Include/Logger.hpp Include/Protocol/VideoInfo.hpp connection.a
	$(CXX) $(CXXFLAGS) $(CXXLIBS) Receiver/Main.cpp Receiver/Receiver.cpp connection.a -o receiver
agent: Agent/Main.cpp Include/Protocol/Packet.hpp
	$(CXX) $(CXXFLAGS) $(CXXLIBS) Agent/Main.cpp -o agent

connection.a: connection.o host.o
	$(AR) $(ARFLAGS) connection.a host.o connection.o
connection.o: Include/Connection/Connection.hpp Connection/Connection.cpp Include/Protocol/Packet.hpp
	$(CXX) $(CXXFLAGS) $(CXXLIBS) Connection/Connection.cpp -c -o connection.o
host.o: Include/Connection/Host.hpp Connection/Host.cpp
	echo $(LD)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) Connection/Host.cpp -c -o host.o

clean:
	rm -f sender receiver agent host.o connection.o connection.a
