#include <chrono>
#include <cstdio>

#include "opencv2/opencv.hpp"

#include "Protocol/VideoInfo.hpp"
#include "Receiver.hpp"

int main(int argc, const char *argv[]) {
  if (argc != 5) {
    printf("Usage: %s [RemoteIp] [RemotePort] [LocalIp] [LocalPort]\n",
           argv[0]);
    return 0;
  }

  Host Remote(argv[1], std::atoi(argv[2]));
  Host Local(argv[3], std::atoi(argv[4]));

  Receiver VideoReceiver(Remote, Local, &std::cout);

  VideoInfo Info;
  VideoReceiver.read(reinterpret_cast<std::byte *>(&Info), sizeof(Info));

  cv::Mat Frame = cv::Mat::zeros(Info.Height, Info.Width, CV_8UC3);
  if (not Frame.isContinuous()) {
    Frame = Frame.clone();
  }
  const size_t Pixels = Frame.total() * Frame.elemSize();
  for (size_t FrameNum = 0; FrameNum < Info.FrameCount; ++FrameNum) {
    auto StartTime = std::chrono::high_resolution_clock::now();

    size_t Received = VideoReceiver.read(reinterpret_cast<std::byte *>(Frame.data), Pixels);
    if (Received != Pixels) {
      assert(VideoReceiver.isClosed());
      break;
    }
    imshow("Video", Frame);

    auto EndTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> ElapsedTime = EndTime - StartTime;

    int WaitTime =
        std::max<int>(1, (1 / Info.Fps - ElapsedTime.count()) * 1000);
    cv::waitKey(WaitTime);
  }
  std::byte End;
  assert(VideoReceiver.read(&End, 1) == 0 and VideoReceiver.isClosed());
  cv::destroyAllWindows();
  return 0;
}
