#include "Receiver.hpp"

Receiver::Receiver(Host RemoteHost, Host BindHost, Logger ActionLogger)
    : Connection(RemoteHost, BindHost), AckNumber(1), BytesRead(),
      Log(ActionLogger), Closed(false) {}

std::optional<Packet::Segment> Receiver::readSegment() {
  auto Packet = recvPacket();
  for (bool CorrectPacketReceived = false; not CorrectPacketReceived;) {
    while (not Packet.has_value()) {
      Packet = recvPacket();
    }

    if (Packet->header.fin == 1) {
      Closed = true;
      Packet::Segment Resp = {};
      Resp.header.syn = Resp.header.fin = 1;
      Log.log("recv\tfin\nsend\tfinack\n");
      sendPacket(Resp);
      return {};
    }

    if (Packet->header.seqNumber == AckNumber) {
      Log.log("recv\tdata\t#", Packet->header.seqNumber, "\n");
      AckNumber += 1;
      CorrectPacketReceived = true;
    } else {
      Log.log("drop\tdata\t#", Packet->header.seqNumber, "\n");
    }

    Packet::Segment AckPacket = {};
    AckPacket.header.ack = 1;
    AckPacket.header.ackNumber = AckNumber - 1;

    Log.log("send\tack\t#", AckPacket.header.ackNumber, "\n");
    sendPacket(AckPacket);

    if (not CorrectPacketReceived) {
      Packet = recvPacket();
    }
  }

  return *Packet;
}

size_t Receiver::read(std::byte *Data, size_t Length) {
  while (BytesRead.size() < Length) {
    auto Segment = readSegment();
    if (not Segment.has_value()) {
      break;
    }
    for (size_t i = 0; i < Segment->header.length; ++i) {
      BytesRead.push(Segment->data[i]);
    }
  }

  Log.log("flush\n");

  size_t BytesWrite = 0;
  while (BytesWrite < Length) {
    if (BytesRead.empty()) {
      break;
    }
    Data[BytesWrite++] = BytesRead.front();
    BytesRead.pop();
  }

  return BytesWrite;
}
