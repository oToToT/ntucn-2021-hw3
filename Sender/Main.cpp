#include <cinttypes>
#include <cstdio>
#include <cstdlib>
#include <string>

#include "opencv2/opencv.hpp"

#include "Protocol/VideoInfo.hpp"
#include "Sender.hpp"

int main(int argc, const char *argv[]) {
  if (argc != 6) {
    printf("Usage: %s [Video] [RemoteIp] [RemotePort] [LocalIp] [LocalPort]\n",
           argv[0]);
    return 0;
  }

  Host Remote(argv[2], std::atoi(argv[3]));
  Host Local(argv[4], std::atoi(argv[5]));
  Sender VideoSender(Remote, Local, &std::cout);

  cv::VideoCapture Video{argv[1]};
  const int Height = Video.get(cv::CAP_PROP_FRAME_HEIGHT);
  const int Width = Video.get(cv::CAP_PROP_FRAME_WIDTH);
  cv::Mat Frame = cv::Mat::zeros(Height, Width, CV_8UC3);
  if (not Frame.isContinuous()) {
    Frame = Frame.clone();
  }
  const size_t Pixels = Frame.total() * Frame.elemSize();

  VideoInfo Info{.Height = Height,
                 .Width = Width,
                 .Fps = Video.get(cv::CAP_PROP_FPS),
                 .FrameCount =
                     static_cast<size_t>(Video.get(cv::CAP_PROP_FRAME_COUNT))};
  VideoSender.write(reinterpret_cast<std::byte *>(&Info), sizeof(Info));

  while (Video >> Frame, not Frame.empty()) {
    VideoSender.write(reinterpret_cast<std::byte *>(Frame.data), Pixels);
  }

  return 0;
}
