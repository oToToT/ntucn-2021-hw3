#include "Sender.hpp"

#include <algorithm>
#include <chrono>

Sender::Sender(Host RemoteHost, Host BindHost, Logger ActionLogger)
    : Connection(RemoteHost, BindHost), SeqNumber(1), Unacked(), Threshold(16),
      WindowSize(1), WindowIncrease(0), Log(ActionLogger), Closed(false) {}

void Sender::recvAck() {
  auto Packet = recvPacket();
  if (not Packet.has_value()) {
    Threshold = std::max(WindowSize / 2, size_t(1));
    WindowSize = 1;
    WindowIncrease = 0;
    Log.log("time\tout,\t\tthreshold = ", Threshold, "\n");

    while (not Unacked.empty()) {
      Resend.push(Unacked.front());
      Unacked.pop();
    }
  } else {
    assert(Packet.has_value());
    Log.log("recv\tack\t#", Packet->header.ackNumber, "\n");
    if (Packet->header.ackNumber >= Unacked.front().header.seqNumber) {
      if (WindowSize < Threshold) {
        WindowSize += 1;
      } else {
        if (++WindowIncrease == WindowSize) {
          WindowSize += 1;
          WindowIncrease = 0;
        }
      }

      while (not Unacked.empty()) {
        if (Packet->header.ackNumber < Unacked.front().header.seqNumber) {
          break;
        }
        Unacked.pop();
      }
    } else {
      // packet loss
    }
  }
}

void Sender::writeSegment() {
  while (Unacked.size() >= WindowSize) {
    recvAck();
  }

  if (not Resend.empty()) {
    Log.log("resnd\tdata\t#", Resend.front().header.seqNumber,
            ",\twinSize = ", WindowSize, "\n");
    sendPacket(Resend.front());
    Unacked.push(Resend.front());
    Resend.pop();
  } else if (not Send.empty()) {
    Log.log("send\tdata\t#", Send.front().header.seqNumber,
            ",\twinSize = ", WindowSize, "\n");
    sendPacket(Send.front());
    Unacked.push(Send.front());
    Send.pop();
  }
}

void Sender::write(const std::byte *Data, size_t Length) {
  Packet::Segment Buffer = {};
  for (size_t i = 0; i < Length; ++i) {
    if (Buffer.header.length == 0) {
      Buffer.header.seqNumber = SeqNumber++;
    }
    Buffer.data[Buffer.header.length++] = Data[i];
    if (Buffer.header.length == Packet::MAX_LENGTH) {
      Send.push(Buffer);
      writeSegment();

      Buffer.header.length = 0;
    }
  }
  if (Buffer.header.length > 0) {
    Send.push(Buffer);
    writeSegment();
  }
}

void Sender::close() {
  if (isClosed()) {
    return;
  }
  Closed = true;
  Log.log("closing\n");

  while (not Send.empty() or not Resend.empty()) {
    Log.log("closing\n");
    writeSegment();
  }

  while (not Unacked.empty()) {
    Log.log("closing\n");
    recvAck();
    writeSegment();
  }

  auto StartTime = std::chrono::high_resolution_clock::now();

  Packet::Segment End = {};
  End.header.fin = 1;
  Log.log("send\tfin\n");
  sendPacket(End);

  auto Resp = recvPacket();
  while (not Resp.has_value()) {
    Log.log("send\tfin\n");
    sendPacket(End);
    Resp = recvPacket();

    auto CurrentTime = std::chrono::high_resolution_clock::now();
    if (std::chrono::duration_cast<std::chrono::seconds>(CurrentTime -
                                                         StartTime)
            .count() >= 5) {
      break;
    }
  }
  if (Resp.has_value()) {
    Log.log("recv\tfinack\n");
  }
}
